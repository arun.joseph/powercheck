#!/bin/bash

export XDG_RUNTIME_DIR="/run/user/1000"

batt_state=$(upower -i `upower -e | grep 'BAT'` | grep state | cut -d':' -f2 | sed -e 's/^[[:space:]]*//')
batt_charge=$(upower -i `upower -e | grep 'BAT'` | grep percentage | cut -d':' -f2 | sed -e 's/^[[:space:]]*//')

counter=0 
while [ "$batt_state" = "fully-charged" -a "$batt_charge" = "100%" -a $counter -lt 10 ]; do
aplay /usr/share/sounds/sound-icons/glass-water-1.wav
sleep 1
counter=$(( $counter + 1 ))
batt_state=$(upower -i `upower -e | grep 'BAT'` | grep state | cut -d':' -f2 | sed -e 's/^[[:space:]]*//')
batt_charge=$(upower -i `upower -e | grep 'BAT'` | grep percentage | cut -d':' -f2 | sed -e 's/^[[:space:]]*//')
echo $counter     
echo `date` " [IN]The battery state is $batt_state" 
echo `date` " [IN]The battery charge is $batt_charge" 
done
echo `date` " [OUT]The battery state is $batt_state" 
echo `date` " [OUT]The battery charge is $batt_charge"