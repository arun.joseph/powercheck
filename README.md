# powercheck

The script is for checking and alerting the user when the battery of the laptop is fully charged. It should be configured as a cron job to be run when ever the laptop is fully charged. 

## Execute Script

The script can be executed by simply running the shell script with below command.

    sh batt_alert.sh

## Cron

To add a cron job first open a crontab editor in nano

    crontab -e

Add the below line to the cron file

    * * * * * [ $(upower -i `upower -e | grep 'BAT'` | grep state | cut -d':' -f2 | sed -e 's/^[[:space:]]*//' | awk '{print $NF}') = 'fully-charged' ] && sh /path/to/script/powercheck/batt_alert.sh > /path/to/script/powercheck/alert.log 2>&1

Save and exit from the editor.

The job will trigger each time the laptop is fully charged and sound the alarm for a few seconds. The alarm would stop as soon as the adapter is disconnected or it times out.